from setuptools import setup, find_packages

setup(
    name='dashboard-covid-19',
    version='0.1.1',
    description='Meltano reports and dashboards for data fetched using the singer tap-covid-19',
    packages=find_packages(),
    package_data={'reports': ['*.m5o'], 'dashboards': ['*.m5o']},
    install_requires=[],
)
